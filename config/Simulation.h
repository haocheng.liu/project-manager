//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef project_config_Simulation_h
#define project_config_Simulation_h

#include "config/Exports.h"

#include "smtk/PublicPointerDefs.h"

namespace config
{

/* ***
 * A base class for applying simulation-specific behavior to projects
 * prior to, or following, certain project events.

*/

class PROJECTCONFIG_EXPORT Simulation
{
public:
  /// Apply changes after project created
  virtual bool postCreate(smtk::project::ProjectPtr project) const { return true; }

  /// Apply changes before project export dialog is displayed
  virtual bool preExport(
    smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
  {
    return true;
  }

protected:
  bool setModelFile(smtk::attribute::FileItemPtr fileItem, smtk::project::ProjectPtr project,
    const std::string& modelIdentifier, smtk::io::Logger& logger) const;
};

} // namespace config

#endif
