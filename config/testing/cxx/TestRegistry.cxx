#include "config/Registry.h"

#include <iostream>

int TestRegistry(int argc, char* argv[])
{
  if (config::Registry::getConfig("truchas") == nullptr)
  {
    std::cerr << "Missing truchas config";
    return 1;
  }

  if (config::Registry::getConfig("xyzzy") != nullptr)
  {
    std::cerr << "Creating wrong config object";
    return 1;
  }

  return 0;
}
