//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectExportBehavior_h
#define pqSMTKProjectExportBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqProjectExportReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectExportReaction(QAction* parent);

  void exportProject() const;

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->exportProject(); }

private:
  Q_DISABLE_COPY(pqProjectExportReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectExportBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectExportBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectExportBehavior() override;

protected:
  pqSMTKProjectExportBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectExportBehavior);
};

#endif
