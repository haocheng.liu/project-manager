//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectLoader.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKRenderResourceBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// Client side
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqRecentlyUsedResourcesList.h"
#include "pqServer.h"
#include "pqServerConfiguration.h"
#include "pqServerConnectDialog.h"
#include "pqServerLauncher.h"
#include "pqServerManagerModel.h"
#include "pqServerResource.h"

// Qt
#include <QDebug>
#include <QMessageBox>
#include <QScopedPointer>
#include <QString>

#define PROJECT_TAG "SMTK_PROJECT"

//-----------------------------------------------------------------------------
static pqSMTKProjectLoader* g_instance = nullptr;

//-----------------------------------------------------------------------------
pqSMTKProjectLoader* pqSMTKProjectLoader::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectLoader(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

//-----------------------------------------------------------------------------
bool pqSMTKProjectLoader::canLoad(const pqServerResource& resource) const
{
  return resource.hasData(PROJECT_TAG);
}

//-----------------------------------------------------------------------------
bool pqSMTKProjectLoader::load(pqServer* server, const QString& path)
{
  // Get project manager and call openProject()
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto& logger = smtk::io::Logger::instance();
  auto project = projectManager->openProject(path.toStdString(), logger);
  if (!project)
  {
    QString msg = QString(logger.convertToString().c_str());
    if (msg.isEmpty())
    {
      msg = QString("Internal error opening project at ") + path;
    }
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed To Create Project"), msg);
    return false;
  }

  // Add project to recently-used list
  pqServerResource resource = server->getResource();
  resource.addData(PROJECT_TAG, "1");
  resource.setPath(path);

  pqRecentlyUsedResourcesList& mruList = pqApplicationCore::instance()->recentlyUsedResources();
  mruList.add(resource);
  mruList.save(*pqApplicationCore::instance()->settings());

  qInfo() << "Opened project" << project->name().c_str();
  emit this->projectOpened(project);

  // Make sure that model resources get rendered, which does *not* happen
  // if the resource panel is hidden.
  auto smtkResourceList = project->resources();
  for (auto smtkResource : smtkResourceList)
  {
    if (smtkResource->isOfType<smtk::model::Resource>())
    {
      pqSMTKResource* pvResource = wrapper->getPVResource(smtkResource);
      if (pvResource == nullptr)
      {
        pvResource = pqSMTKRenderResourceBehavior::instance()->createPipelineSource(smtkResource);
      }
      pqSMTKRenderResourceBehavior::instance()->renderPipelineSource(pvResource);
    } // if (model resource)
  }   // for (smtk resources)

  return true;
}

//-----------------------------------------------------------------------------
bool pqSMTKProjectLoader::load(const pqServerResource& resource)
{
  const pqServerResource server = resource.schemeHostsPorts();

  pqServerManagerModel* smModel = pqApplicationCore::instance()->getServerManagerModel();
  pqServer* pq_server = smModel->findServer(server);
  if (!pq_server)
  {
    int ret =
      QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Disconnect from current server?"),
        tr("The file you opened requires connecting to a new server. \n"
           "The current connection will be closed.\n\n"
           "Are you sure you want to continue?"),
        QMessageBox::Yes | QMessageBox::No);
    if (ret == QMessageBox::No)
    {
      return false;
    }
    pqServerConfiguration config_to_connect;
    if (pqServerConnectDialog::selectServer(
          config_to_connect, pqCoreUtilities::mainWidget(), server))
    {
      QScopedPointer<pqServerLauncher> launcher(pqServerLauncher::newInstance(config_to_connect));
      if (launcher->connectToServer())
      {
        pq_server = launcher->connectedServer();
      }
    }
  }

  if (pq_server)
  {
    return this->load(pq_server, resource.path());
  }

  // (else)
  return false;
}

//-----------------------------------------------------------------------------
pqSMTKProjectLoader::pqSMTKProjectLoader(QObject* parent)
  : Superclass(parent)
{
}

//-----------------------------------------------------------------------------
pqSMTKProjectLoader::~pqSMTKProjectLoader()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

#undef PROJECT_TAG
