//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "Truchas.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/common/UUID.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"

#include "boost/filesystem.hpp"

#include <iostream>
#include <sstream>
#include <vector>

namespace config
{

bool Truchas::postCreate(smtk::project::ProjectPtr project) const
{
  std::cout << "Enter Truchas::postCreate()" << std::endl;

  // Relabel side sets to include side set number
  // This is an interim hack until Resource Panel can display pedigree ids
  auto modelResource = project->findResource<smtk::model::Resource>("default");
  if (modelResource)
  {
    std::stringstream ss;

    std::cout << "Renaming model volumes" << std::endl;
    auto volumeRefs = modelResource->findEntitiesOfType(smtk::model::VOLUME, true);
    for (auto volumeRef : volumeRefs)
    {
      auto uuid = volumeRef.entity();
      auto propList = modelResource->integerProperty(uuid, "pedigree id");
      if (propList.size() == 1)
      {
        int id = propList[0];
        ss.str("");
        ss << "block " << id << " - " << volumeRef.name();
        volumeRef.setName(ss.str());
      } // if
    }   // for (volumeRef)

    std::cout << "Renaming model faces" << std::endl;
    auto faceRefs = modelResource->findEntitiesOfType(smtk::model::FACE, true);
    for (auto faceRef : faceRefs)
    {
      auto uuid = faceRef.entity();
      auto propList = modelResource->integerProperty(uuid, "pedigree id");
      if (propList.size() == 1)
      {
        int id = propList[0];
        ss.str("");
        ss << "sideset " << id << " - " << faceRef.name();
        faceRef.setName(ss.str());
      } // if
    }   // for (faceRef)
  }     // if (modelResource)

  // If second mesh was loaded, assign to induction heating component item
  auto altModelResource = project->findResource<smtk::model::Resource>("second");
  auto attResource = project->findResource<smtk::attribute::Resource>("default");
  if (altModelResource && attResource)
  {
    const std::string emName = "electromagnetics";
    auto emAtt = attResource->findAttribute(emName);
    if (!emAtt)
    {
      emAtt = attResource->createAttribute(emName, emName);
    }
    if (emAtt)
    {
      auto modelItem = emAtt->findComponent("model");
      if (modelItem)
      {
        // Get model entity(ies)
        auto uuids = altModelResource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY, true);
        if (uuids.size() != 1)
        {
          std::cerr << "Unexpected number of models in " << altModelResource->location()
                    << ", expected 1, got " << uuids.size() << std::endl;
        }
        else
        {
          auto uuid = *(uuids.begin());
          auto model = altModelResource->find(uuid);
          if (model)
          {
            altModelResource->setIntegerProperty(uuid, emName, 1);
            modelItem->setObjectValue(model);
          }
        } // if (1 model)
      }   // if (modelItem)
    }     // if (electromagnetics att)
  }       // if (attResource)

  return true;
}

bool Truchas::preExport(
  smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
{
  int paramVersion = exportOp->parameters()->definition()->version();
  if (paramVersion == 0)
  {
    // Set project name
    auto projectNameItem = exportOp->parameters()->findString("ProjectName");
    if (projectNameItem)
    {
      projectNameItem->setValue(project->name());
    }
    return true;
  } // paramVersion 0

  if (paramVersion > 1)
  {
    std::cerr << "Unsupported parameters version " << paramVersion;
    return false;
  }

  // (else) paramVersion 1
  boost::filesystem::path projectDir(project->directory());

  // Model
  auto modelItem = exportOp->parameters()->findComponent("model");
  if (modelItem)
  {
    auto modelResource = project->findResource<smtk::model::Resource>("default");

    // Get all models in model rsource
    auto uuids = modelResource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY, true);
    std::vector<smtk::resource::ComponentPtr> modelList;
    for (const auto& uuid : uuids)
    {
      auto model = modelResource->find(uuid);
      if (model)
      {
        modelList.push_back(model);
      }
    } // for

    if (modelList.size() == 1)
    {
      modelItem->setValue(modelList[0]);
    }
    else
    {
      smtkWarningMacro(exportOp->log(), "Unable to assign model because"
                                        " the number of models in the project is "
          << modelList.size());
    }
  }

  // Output file
  auto inpFileItem = exportOp->parameters()->findFile("output-file");
  if (inpFileItem)
  {
    std::string inpFileName = project->name() + ".inp";
    boost::filesystem::path inpFilePath = projectDir / "export" / inpFileName;
    inpFileItem->setValue(0, inpFilePath.string());
  }

  // Mesh file
  auto meshFileItem = exportOp->parameters()->findFile("mesh-file");
  this->setModelFile(meshFileItem, project, "default", exportOp->log());

  // Altmesh file (when induction heating enabled)
  auto altMeshFileItem = exportOp->parameters()->findFile("alt-mesh-file");
  auto simatts = project->findResource<smtk::attribute::Resource>("default");
  if (this->isInductionHeatingEnabled(simatts))
  {
    this->setModelFile(altMeshFileItem, project, "second", exportOp->log());
  }

  return true;
}

bool Truchas::isInductionHeatingEnabled(smtk::attribute::ResourcePtr& attResource) const
{
  // Check attribute resource entities
  if (attResource == nullptr)
  {
    return false;
  }

  // Get "analysis" att
  auto analysisAtt = attResource->findAttribute("analysis");
  if (analysisAtt == nullptr)
  {
    return false;
  }

  // Get "Heat Transfer" item
  auto htItem = analysisAtt->findGroup("Heat Transfer");
  if (htItem == nullptr)
  {
    return false;
  }

  // Get "Induction Heating" item
  auto ihItem = htItem->find("Induction Heating");
  if (ihItem == nullptr)
  {
    return false;
  }

  return ihItem->isEnabled();
}

} // namespace config
