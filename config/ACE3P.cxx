//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "ACE3P.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/common/UUID.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"

#include "boost/filesystem.hpp"

#include <iostream>
#include <sstream>
#include <vector>

namespace config
{

bool ACE3P::postCreate(smtk::project::ProjectPtr project) const
{
  std::cout << "Enter ACE3P::postCreate() (placeholder)" << std::endl;
  return true;
}

bool ACE3P::preExport(
  smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
{
  int paramVersion = exportOp->parameters()->definition()->version();
  if (paramVersion > 1)
  {
    std::cerr << "Unsupported parameters version " << paramVersion;
    return false;
  }

  // Model
  auto modelItem = exportOp->parameters()->findComponent("model");
  if (modelItem)
  {
    auto modelResource = project->findResource<smtk::model::Resource>("default");

    // Get all models in model resource
    auto uuids = modelResource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY, true);
    std::vector<smtk::resource::ComponentPtr> modelList;
    for (const auto& uuid : uuids)
    {
      auto model = modelResource->find(uuid);
      if (model)
      {
        modelList.push_back(model);
      }
    } // for

    if (modelList.size() == 1)
    {
      modelItem->setValue(modelList[0]);
    }
    else
    {
      smtkWarningMacro(exportOp->log(), "Unable to assign model because"
                                        " the number of models in the project is "
          << modelList.size());
    }
  }

  // Mesh file
  auto meshFileItem = exportOp->parameters()->findFile("MeshFile");
  this->setModelFile(meshFileItem, project, "default", exportOp->log());

  // Output folder and filename
  auto outFolderItem =
    exportOp->parameters()->itemAtPathAs<smtk::attribute::DirectoryItem>("Analysis/OutputFolder");
  if (outFolderItem == nullptr)
  {
    std::cerr << "INTERNAL ERROR - Export paramters missing OutputFolder item";
    return false;
  }

  boost::filesystem::path projectDir(project->directory());
  boost::filesystem::path outFolderPath = projectDir / "export";
  outFolderItem->setValue(0, outFolderPath.string());

  // Set filename to project name plus analysis type
  auto outPrefixItem =
    exportOp->parameters()->itemAtPathAs<smtk::attribute::StringItem>("Analysis/OutputFilePrefix");
  if (outPrefixItem == nullptr)
  {
    std::cerr << "INTERNAL ERROR - Export paramters missing OutputFilePrefix item";
    return false;
  }
  outPrefixItem->setValue(project->name());

  return true;
}

} // namespace config
